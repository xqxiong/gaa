package com.telenav.automation.services;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * Created by xqxiong on 12/12/2016.
 */
public class StartServer {
    public static void main(String[] args) {
        String webPort = System.getenv("PORT");
        if (webPort == null || webPort.isEmpty()) {
            webPort = "8083";
        }

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        final Server server = new Server(Integer.valueOf(webPort));
        ServletHolder jerseyServlet = context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
        // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.classnames",
                RegressionExService.class.getCanonicalName());
        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.packages",
                "com.jersey.series.json.service");
        jerseyServlet.setInitParameter(
                "javax.ws.rs.Application",
                "com.telenav.automation.services.ServerApplication");
        server.setHandler(context);

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            server.destroy();
        }

    }
}