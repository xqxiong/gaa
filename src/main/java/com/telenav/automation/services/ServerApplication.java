package com.telenav.automation.services;

/**
 * Created by xqxiong on 12/23/2016.
 */

import org.glassfish.jersey.server.ResourceConfig;
import com.owlike.genson.ext.jaxrs.GensonJsonConverter;

public class ServerApplication extends ResourceConfig {
    public ServerApplication() {
        super();
        register(GensonJsonConverter.class);
    }
}