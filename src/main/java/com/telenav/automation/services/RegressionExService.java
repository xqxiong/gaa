package com.telenav.automation.services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.telenav.automation.interfaces.GAARequest;
import com.telenav.automation.interfaces.GAAResponse;
import com.televigation.regressionex.assertions.AssertMethod;
import com.televigation.regressionex.assertions.AssertMethodFactory;
import com.televigation.regressionex.assertions.AssertionResult;
import com.televigation.regressionex.conditions.*;
import com.televigation.regressionex.datatypes.TestResultRecord;
import com.televigation.regressionex.datatypes.TestValue;

import java.util.Map;
import java.util.Vector;


/**
 * Created by xqxiong on 12/12/2016.
 */
@Path("/")
public class RegressionExService {
    /**
     122xx	Server errors
     12210	Internal server error
     12220	Authentication failed
     123xx	Service responses
     12300	Pass validation
     12310	Validation failed
     12321	Missing mandatory input route_1/route_2
     12322	Invalid input - route_1
     12323	Invalid response type. Must be image/json.
     */
    public static String CODE_INTERNAL_ERROR = "12210";
    public static String CODE_AUTH_FAILED = "12220";
    public static String CODE_VALIDATION_PASS = "12300";
    public static String CODE_VALIDATION_FAIL = "12310";
    public static String CODE_VALIDATIOM_ERROR = "12320";

    @GET
    @Path("/test")
    @Produces("text/plain")
    public String getString() {
        return "TEST!";
    }

    @GET
    @Path("/hello")
    @Produces("text/plain")
    public String getString2() {
        return "HELLO! I am RegressionEx web service!";
    }

    @GET
    @Path("/conditions/{conditionName}/{filedName}")
    @Produces({MediaType.APPLICATION_XML, MediaType .APPLICATION_JSON})
    public String conditions(@PathParam("conditionName") String name,
                       @DefaultValue("nofield") @PathParam("filedName") String nameP,
                       @DefaultValue("java.lang.String") @QueryParam("type") String typeP,
                       @QueryParam("value") String valueP,
                       @QueryParam("ref") String ref,
                       @QueryParam("out") String out) throws Exception{

        Condition oCondition;
        if (name.equals("equals")) {
            oCondition = ConditionFactory.getInstance().getCondition(name, null);
        } else {
            Object oInitVal = (new TestValue(nameP, valueP, typeP)).getValueObject();
            oCondition = ConditionFactory.getInstance().getCondition(name, oInitVal);
        }
        if(oCondition != null) {
            TestValue oRefValue = new TestValue(nameP, ref, typeP);
            TestValue oOutValue = new TestValue(nameP, out, typeP);

            if(oCondition.compare(oRefValue.getValueObject(), oOutValue.getValueObject())) {
                return "Condition name is " + name + "; param is " + valueP +
                      "; ref is " + ref + ";" + " out is " + out +
                        "; result is Pass. label is " + oCondition.getLabel();
            }
            else {
                return "Condition name is " + name + "; param is " + valueP +
                        "; ref is " + ref + ";" + " out is " + out +
                        "; result is Fail. label is " + oCondition.getLabel();
            }
        }
        //return "Condition name is " + name + "; ref is " + ref + ";" + " out is " + out;
        return "UNKNOWN";
    }

    @GET
    @Path("/assertions/{assertionName}")
    @Produces(MediaType.APPLICATION_JSON)
    public GAAResponse assertions(@PathParam("assertionName") String name,
                                  @QueryParam("ref") String ref,
                                  @QueryParam("out") String out,
                                  @DefaultValue("nofield") @QueryParam("field") String field,
                                  @DefaultValue("") @QueryParam("value") String value,
                                  @DefaultValue("all") @QueryParam("scope") String scope,
                                  @DefaultValue("") @QueryParam("addition") String addition) throws Exception {

        return getGAAResponse(name, ref, out, field, value, scope, addition);
    }


    @POST
    @Path("/assertions/{assertionName}")
    @Produces(MediaType.APPLICATION_JSON)
    public GAAResponse add(@PathParam("assertionName") String name,
                       @QueryParam("ref") String ref,
                       @QueryParam("out") String out,
                       @DefaultValue("nofield") @QueryParam("field") String field,
                       @DefaultValue("") @QueryParam("value") String value,
                       @DefaultValue("all") @QueryParam("scope") String scope,
                       @DefaultValue("") @QueryParam("addition") String addition) throws Exception {
        // TODO for search demo only
        return getGAAResponse(name, ref, out, field, value, scope, addition);
    }

    private GAAResponse getGAAResponse(String name, String ref, String out, String field, String value, String scope, String addition) throws Exception {
        AssertionResult result = getAssertionResult(name, ref, out, field, value, scope, addition);
        if(result.isAssertionResultPass())
            return new GAAResponse(CODE_VALIDATION_PASS, result.getAssertionRuleLabel(), result.getAssertionResultMessage());
        else{
            return new GAAResponse(CODE_VALIDATION_FAIL, result.getAssertionRuleLabel(), result.getAssertionResultMessage());
        }
    }

    private AssertionResult getAssertionResult(String name, String ref, String out, String field,
                                      String value, String scope, String addition) throws Exception {
        // TODO for search demo only
        AssertMethod oAssertMethod = AssertMethodFactory.getInstance().getAssertMethod(name);
        String[] outList = out.split(";");
        Vector<TestResultRecord> inScopeRecords = new Vector<TestResultRecord>();
        for(String outStr : outList){
            TestResultRecord outRecord = new TestResultRecord("");
            TestValue outVal = new TestValue(field, outStr);
            outRecord.addResult(outVal);
            inScopeRecords.add(outRecord);
        }

        AssertionResult assertionResult = oAssertMethod.assertResult(null, field, value, scope, inScopeRecords, addition, null, null);
        System.out.println("assertion name is " + oAssertMethod.getName() + "; label is " + assertionResult.getAssertionRuleLabel() + "; message is " + assertionResult.getAssertionResultMessage());

        return assertionResult;
    }

    @POST
    @Path("/assertion/search/value_in_each")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GAAResponse postSearchRequest(GAARequest request) throws Exception {
        // TODO for search demo only
        System.out.println(request.toString());
        String name = "contains";
        String ref = "";
        String field = request.getField();
        StringBuffer outsb = new StringBuffer();
        Object[] results = request.getResults();
        for (Object oResult: results) {
            Map<String, String> result = (Map<String, String>) oResult;
            if (result.containsKey(field))
                outsb.append(result.get(field));
            outsb.append(";");
        }
        String out = outsb.substring(0, outsb.length() - 1);
        System.out.println(out);
        String value = request.getValue();
//        AssertionResult result = getAssertionResult(name, ref, out, field, value, scope, addition);
        return getGAAResponse(name, ref, out, field, value, "all", "");
        //return new GAAResponse();
    }
}
