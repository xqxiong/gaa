package com.telenav.automation.interfaces;

import com.owlike.genson.Genson;

/**
 * Created by xqxiong on 12/22/2016.
 */
public class GAAResponse{

    public GAAResponse(){
        this.response_code = "test response code";
        this.response_message = "test response message";
        this.validate_label = "test validation label";
    }

    public GAAResponse(String code, String label, String message){
        this.response_code = code;
        this.response_message = message;
        this.validate_label = label;
    }

    private String response_code;
    public String getResponseCode() {
        return response_code;
    }

    public void setResponseCode(String response_code) {
        this.response_code = response_code;
    }

    private String response_message;
    public String getResponseMessage() {
        return response_message;
    }
    public void setResponseMessage(String response_message) {
        this.response_message = response_message;
    }

    private String validate_label;
    public String getValidateLabel() {
        return validate_label;
    }
    public void setValidateLabel(String validate_label) {
        this.validate_label = validate_label;
    }
    public String toString(){
        Genson genson = new Genson();
        return genson.serialize(this);
    }
    public static GAAResponse fromString(String strJson){
        Genson genson = new Genson();
        return genson.deserialize(strJson, GAAResponse.class);
    }
}
