package com.telenav.automation.interfaces;

import java.util.Map;

/**
 * Created by xqxiong on 1/6/2017.
 */
public class GAARequestTestResults {
    GAARequestTestResults(){
        this.results = new Integer[3];
        this.results[0] = 1;
        this.results[1] = 2;
        this.results[2] = 3;
    }
    private Object[] results;
    public Object[] getResults() {
        return results;
    }
    public void setResults(Object[] results) {
        this.results = results;
    }
}
