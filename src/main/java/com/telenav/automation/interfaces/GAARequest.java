package com.telenav.automation.interfaces;

import com.owlike.genson.Genson;

/**
 * Created by xqxiong on 1/6/2017.
 */
public class GAARequest {
    private String field;
    public String getField(){
        return this.field;
    }
    public void setField(String field){
        this.field = field;
    }
    private String value;
    public String getValue(){
        return this.value;
    }
    public void setValue(String value){
        this.value = value;
    }
    private Boolean bExactMatch;
    public Boolean getExactMatch(){
        return this.bExactMatch;
    }
    public void setExactMatch(Boolean bMatch){
        this.bExactMatch = bMatch;
    }
    private Boolean bCaseSensitive;
    public Boolean getbCaseSensitive(){
        return this.bCaseSensitive;
    }
    public void setbCaseSensitive(Boolean case_sensitive){
        this.bCaseSensitive = case_sensitive;
    }
    public GAARequest(){

    }
    private Object[] results;
    public Object[] getResults(){
        return this.results;
    }
    public void setResults(Object[] results){
        this.results = results;
    }

    private Object[] aliases;
    public Object[] getaliases(){
        return this.aliases;
    }
    public void setAliases(Object[] aliases){
        this.aliases = aliases;
    }

    public String toString(){
        Genson genson = new Genson();
        return genson.serialize(this);
    }
    public static GAARequest fromString(String strJson){
        Genson genson = new Genson();
        return genson.deserialize(strJson, GAARequest.class);
    }
}
